# Wheel of Fortune (Guess the word Game) with Vector

![fortune_wheel](public/fortune_wheel.jpg)

## The point of the game / _Суть игры_

You get a random word in English. You need to guess it, and 
your Anki Vector Robot will help you.
You see a number of empty cells by the number of letters of 
the hidden word, as well as its translation into Russian.
Your move: enter the selected letter using the keyboard.
Then Vector will make his move and name the letter he has chosen.

_При старте игры будет загадано случайное слово на английском языке.
Вам нужно его угадать, и ваш Вектор вам в этом поможет.
Вы увидите пустые клетки в количестве, соответствующем буквам 
загаданного слова, а также перевод этого слова на русский язык.
Ваш ход: выберите букву, нажав нужную клавишу на клавиатуре.
Затем Vector сделает свой ход и назовет выбранную им букву._

![fortune_wheel_main](public/fortune_wheel.png)

Vector will select letters according to the frequency of 
their occurrence in the English language. He will try
to call common letters more often.

_Вектор будет выбирать буквы в сответствии с частотой их встречаемости 
в английском языке. Распространенные буквы он постарается 
называть чаще._

There are no scores and losers in the game, but rest assured, 
Vector will be very happy if he guesses the last letter.

_В этой игре нет счета или проигравших, но будьте уверены, 
Вектор будет очень рад, если последнюю букву угадает он._

Learn more about Vector: https://www.anki.com/en-us/vector

Learn more about the SDK: https://developer.anki.com/

## How to start the app / _Как запустить приложение_

### 0. Set up Vector / _Настройка Вектора_

If you haven't set up your Vector yet and don't know how to do it, follow all
[instructions](https://developer.anki.com/vector/docs/index.html).
Also create and run a virtual environment for game project and download the requirements:

_Если ваш Вектор еще не был настроен, и вы не знаете, как это сделать, используйте
руководство от разработчиков по ссылке [instructions](https://developer.anki.com/vector/docs/index.html).
Также вам потребуется создать и запустить виртуальное окружение для проекта и загрузить
необходимые пакеты:_

```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

### 1. Virtual environment / _Виртуальное окружение_

You should create an .env file in the game directory and put the following value in it:

_Следует создать файл .env в директории игры и поместить в него следующую строку:_
```
FLASK_SECRET_KEY = wefbewjhb67t6uew
```
___Do not use these key, it is given only as an example and will not work___

___Не используйте этот секретный ключ, он не будет работать и приведен только как пример___

FLASK_SECRET_KEY must be created manually.
There are many ways, one of them is to use the following code in the Python Console:

_FLASK_SECRET_KEY может быть создан вручную. Есть много вариантов для этого, например,
использовать в консоли следующий код:_

```python
import os
os.urandom(12)
```

### 2. Get RANDOM_WORD_KEY / _Получить ключ RANDOM_WORD_KEY_

RANDOM_WORD_KEY - API key to getting a random English word to be guessed.
You need to register [here](https://api-ninjas.com/register), confirm your email and log in.
Then you can see API key in [your profile](https://api-ninjas.com/profile) - just use the Show API Key button.
Copy it to your .env file into the appropriate variable.
You can get 50,000 words per month for free.

_RANDOM_WORD_KEY - API ключ для получения случайных слов на 
английском языке, которые можно угадывать.
Вам надо зарегистрироваться [здесь](https://api-ninjas.com/register),
потвердить ваш адрес электронной почты и залогиниться.
Затем вы увидите ваш API ключ в [своем профиле](https://api-ninjas.com/profile) - просто 
используйте кнопку Show API Key.
Скопируйте ключ в ваш .env файл и подставьте в соотвествующую переменную.
Вы можете получить бесплатно до 50,000 слов в месяц._

### 3. Get RAPID_API_KEY / _Получить ключ RAPID_API_KEY_

RAPID_API_KEY - API key to getting a translation of a random hidden word.
It will be given for free after registration on the [rapidapi.com](https://rapidapi.com/) 
(you can use your Google account). Then follow 
[Microsoft Translator Text](https://rapidapi.com/microsoft-azure-org-microsoft-cognitive-services/api/microsoft-translator-text/).
Click Subscribe to Test and select a free plan. Then return to the API page and 
find on the page the __X-RapidAPI-Key__ and copy its value to your .env file into the appropriate variable.
You can get 500,000 characters per month for free.

_RAPID_API_KEY - API ключ для получения перевода случайного 
загаданного слова. Он будет получен бесплатно после регистрации 
на [rapidapi.com](https://rapidapi.com/) 
(вы можете использовать для этого свой Google аккаунт). 
Затем перейдите на [Microsoft Translator Text](https://rapidapi.com/microsoft-azure-org-microsoft-cognitive-services/api/microsoft-translator-text/).
Нажмите Subscribe to Test и выберите free plan. 
Далее вернитесь на страницу API page и найдите на ней
__X-RapidAPI-Key__ и скопируйте его в свой .env файл как значение
соответствующей переменной.
Вы можете получить бесплатно до 500,000 символов в месяц._

### 4. Launch game / _Запуск игры_

Use command

_Используйте команду_

```
python3 run.py
```
in terminal or run the run.py file in any convenient way.

_в терминале или запустите файл run.py удобным вам способом._

### I hope your Vector likes the new game!

### _Надеюсь, вашему Вектору понравится новая игра!_


## Privacy Policy and Terms and Conditions

Use of Vector and the Vector SDK is subject to Anki's [Privacy Policy](https://www.anki.com/en-us/company/privacy) and [Terms and Conditions](https://www.anki.com/en-us/company/terms-and-conditions).

## License
<p><a name="license"></a></p>

Distributed under the MIT License.
