# !/home/marina/PycharmProjects/Vector-robot/Envs/bin/python3
"""Start the game."""
import logging
import logging.config
import os
import sys
import webbrowser
from threading import Thread
from time import sleep

import anki_vector

from config import GAME_IP, GAME_PORT, VECTOR_IP

from dotenv import load_dotenv

from fortune_wheel import flask_app
from fortune_wheel.config import LOGGING_CONFIG

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

RAPID_API_KEY = os.environ.get('RAPID_API_KEY')
flask_app.config['SECRET_KEY'] = os.environ.get('FLASK_SECRET_KEY')
logging.config.dictConfig(LOGGING_CONFIG)

logger = logging.getLogger(__name__)


class VectorImportClass:
    """The class required to be able to import a robot object."""

    def __init__(self, robot):
        """
        Initialize robot object.

        Args:
            robot (anki_vector.AsyncRobot): async robot control object.
        """
        self.vector = robot


class RunFlask:
    """
    Class to run Flask game app.

    Redesigned flask_helpers mini library by the Vector developers.
    I took from it only what we need for the game.

    Attributes:
        enable_flask_logging (bool): flag indicating whether logging
                                     is necessary.
        open_page (bool): flag indicating whether open page is necessary.
        open_page_delay (float): delay of page opening (seconds).
        new_flag (int): flag indicating whether new window is necessary.
        auto_raise (bool): flag indicating that you need to open a window
                           in the foreground.
        running (bool): flag indicating app status (running or not).
    """

    enable_flask_logging = False
    open_page = True
    open_page_delay = 1.0
    new_flag = 0
    auto_raise = True
    running = False

    def __init__(self, app, host_ip, host_port):
        """
        Initialize new game flask object.

        Args:
            app (fortune_wheel.Flask): flask game object.
            host_ip (str): game host.
            host_port (int): game port number.
        """
        self.app = app
        self.ip = host_ip
        self.port = str(host_port)

    def run_flask(self):
        """
        Run the Flask webserver on specified host and port.

        Optionally also open that same host:port page in your browser
        to connect. Also, disable logging in Flask
        (it's enabled by default).
        """
        if not self.enable_flask_logging:
            logger.setLevel(logging.ERROR)
        if self.open_page:
            self.delayed_open_web_browser(f'http://{self.ip}:{self.port}')
        self.app.run(
            host=self.ip, port=self.port, use_evalex=False, threaded=True,
            use_reloader=False
        )

    def delayed_open_web_browser(self, url, specific_browser=None):
        """
        Start thread, add delay to open the game page.

        We add a delay (dispatched in another thread) to open the page
        so that the flask webserver is open before the webpage requests
        any data.
        On OSX the following would use the Chrome browser app
        from that location:
        specific_browser = 'open -a /Applications/Google Chrome.app %s'.

        Args:
            url (str): game address.
            specific_browser (str): command for the browser
                                    if we need to specify.

        Returns:
            None:
        """
        thread = Thread(
            target=self.sleep_and_open_web_browser,
            kwargs=dict(url=url, specific_browser=specific_browser)
        )
        thread.daemon = True
        thread.start()
        self.running = True

    def sleep_and_open_web_browser(self, url, specific_browser):
        """
        Open browser game window.

        Method that passed to the Thread constructor.

        Args:
            url (str): game url.
            specific_browser (str):command for the browser if need to specify.

        Returns:
            None:
        """
        sleep(self.open_page_delay)
        browser = webbrowser
        if specific_browser:
            browser = webbrowser.get(specific_browser)
        browser.open(url, new=self.new_flag, autoraise=self.auto_raise)


def run(current_app=flask_app, count_connections=1):
    """
    Start the game and establish a connection with the robot.

    Args:
        current_app (Flask): flask game object.
        count_connections (int): number of current connection attempt.

    Returns:
        None:
    """
    game_process = RunFlask(current_app, GAME_IP, GAME_PORT)
    while not game_process.running:
        try:
            if count_connections >= 10:
                print(
                    'Соединение не состоялось. Возможно, Вектор '
                    'устал и ему нужно подзарядиться...'
                )
                break
            print(
                f'Попытка #{count_connections} '
                'установки соединения с роботом...'
            )
            args = anki_vector.util.parse_command_args()
            with anki_vector.AsyncRobot(
                    args.serial, ip=VECTOR_IP, enable_face_detection=True,
                    enable_custom_object_detection=True
            ) as robot:
                current_app.vector_import = VectorImportClass(robot)
                robot.behavior.say_text("Let's play!")
                game_process.run_flask()
        except Exception as ex:
            template = 'An exception of type {0} occurred. Arguments:\n{1!r}'
            message = template.format(type(ex).__name__, ex.args)
            logger.error(message)
            print(
                f'Попытка #{count_connections} установки соединения с '
                'роботом не удалась. Пробуем еще раз...'
            )
            count_connections += 1


if __name__ == '__main__':
    try:
        run()
    except KeyboardInterrupt:
        print(
            'If you want to finish the game, '
            'click Finish in the web application.'
        )
    except anki_vector.exceptions.VectorConnectionException as e:
        sys.exit(f'A connection error occurred: {e}')
