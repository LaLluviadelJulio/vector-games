"""Initialization of the Flask application for game and imports."""
import os
import sys

from config import FLASK_CONFIG

from dotenv import load_dotenv

from flask_caching import Cache


dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

try:
    from flask import Flask
except ImportError:
    sys.exit(
        'Cannot import from flask: Do `pip3 install --user flask` to install'
    )


loto_app = Flask(__name__)
loto_app.config['SECRET_KEY'] = os.environ.get('FLASK_SECRET_KEY')
loto_app.config.from_mapping(FLASK_CONFIG)

# creating a cache instance
cache = Cache(loto_app)


from russian_loto import views # noqa
