"""Stores game settings."""
from datetime import datetime

# flask
FLASK_CONFIG = {
    'DEBUG': True,
    'CACHE_TYPE': 'SimpleCache',
    'CACHE_DEFAULT_TIMEOUT': 300
}

# game address
GAME_IP = '127.0.0.1'
GAME_PORT = 5000

# Vector settings
VECTOR_IP = '192.168.0.103'

# logging
LOGGING_LEVEL = 'DEBUG'
LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'colored_console': {
            '()': 'coloredlogs.ColoredFormatter',
            'format': '%(asctime)s - %(name)s - %(levelname)s - %(message)s',
            'datefmt': '%H:%M:%S'
        },
        'format_for_file': {
            'format': '%(asctime)s :: %(levelname)s :: %(funcName)s in '
                      '%(filename)s (l:%(lineno)d) :: %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S'
        },
    },
    'handlers': {
        'console': {
            'level': LOGGING_LEVEL,
            'class': 'logging.StreamHandler',
            'formatter': 'colored_console',
            'stream': 'ext://sys.stdout'
        },
        'file': {
            'level': LOGGING_LEVEL,
            'class': 'logging.FileHandler',
            'formatter': 'format_for_file',
            'filename': f'logs/{LOGGING_LEVEL}_{datetime.today().date()}.log'
        }
    },
    'loggers': {
        '': {
            'level': LOGGING_LEVEL,
            'handlers': ['console', 'file'],
        },
    },
}

# robot phrases during the game
JOY_PHRASES = [
    'Great!', 'Good number!',
    'Not bad!', 'Another one!',
    'My fortune!', 'Yes!'
]

ROBOT_WINS = [
    'I knew it was my day!',
    'Who won? I won!',
    'What a lucky guy I am!',
    'I love this game!'
]

ROBOT_LOSES = [
    "It's not fair", "I'll be lucky next time",
    'Congratulations, you win', 'Stupid game! Phew!'
]
