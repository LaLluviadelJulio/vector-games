"""Stores game management and card creation classes."""
import logging
import random

from russian_loto import cache
from russian_loto.config import (
    JOY_PHRASES, ROBOT_LOSES, ROBOT_WINS
)
from russian_loto.run import loto_app

logger = logging.getLogger(__name__)


class LotoGameManager:
    """
    Process a game.

    Attributes:
        _loto_max_num (int): max game number.
    """

    _loto_max_num = 90

    def __init__(self):
        """Initialize game manager object."""
        #: list of int: numbers that have already come in during the game.
        self.called_numbers_list = []
        #: int: numbers that haven't come in yet (at the start - from 1 to 90).
        self.not_called_number_list = list(range(1, self._loto_max_num + 1))
        #: bool: flag of the game status.
        self.game_over = False
        #: int: game speed.
        self.game_speed = cache.get('game_speed')
        # #: bool: flag of the Vector game voiceover status.
        self.voiceover = cache.get('play_with_robot')
        # robot's cards numbers
        self.robot_nums = cache.get('robot_nums')

    def call_a_number(self, pseudo_random_number=None):
        """
        Call the current number.

        Vector calls the current incoming number as if it were the host
        and pulled the numbers out of the game bag.

        Returns:
            tuple [int, bool]: current number, flag whether the length
                               of the list of named numbers is equal to
                               the largest number.
        """
        try:
            while not self.game_over:
                pseudo_random_inx = random.randrange(
                    len(self.not_called_number_list)
                )
                pseudo_random_number = self.not_called_number_list.pop(
                    pseudo_random_inx
                )
                if pseudo_random_number not in self.called_numbers_list:
                    self.called_numbers_list.append(pseudo_random_number)
                    if not self.not_called_number_list:
                        self.game_over = True
                    break
            if self.voiceover:
                if self.game_speed >= 2:
                    vector = loto_app.vector_import.vector
                    text = str(pseudo_random_number)
                    if self.game_speed >= 3:
                        text = self.robot_emotions(pseudo_random_number, text)
                    vector.behavior.say_text(text)

            return (
                pseudo_random_number, self.game_over,
                self.called_numbers_list
            )
        except ValueError as err:
            template = 'An exception of type {0} occurred. Arguments:\n{1!r}'
            message = template.format(type(err).__name__, err.args)
            logger.error(message)

    def robot_emotions(self, current_num, initial_text):
        """
        Force robot emotion reaction.

        Args:
            current_num (int): current number of the game move.
            initial_text (str): initial text to be voiced by the robot.

        Returns:
            str: text to be voiced by the robot
        """
        vector = loto_app.vector_import.vector
        if bool(random.getrandbits(1)):
            if current_num in self.robot_nums:
                vector.anim.play_animation_trigger('PettingLevel1')
                return f'{initial_text}, {random.sample(JOY_PHRASES, k=1)[0]}'
            vector.anim.play_animation_trigger('ICantDoThat')
        return initial_text

    @staticmethod
    def robot_end_game_emotions(player_wins, robot_wins):
        """
        Force robot emotion reaction when the game is over.

        Args:
            player_wins (str): player's result of the game.
            robot_wins (str): robot's result of the game.

        Returns:
            str: name of the robot's reaction.
        """
        vector = loto_app.vector_import.vector
        robot_reaction = 'GreetAfterLongTime'
        logger.info(
            f'Game result: player wins - {player_wins}, '
            f'robot wins - {robot_wins}'
        )
        if player_wins == 'true':
            if robot_wins == 'true':
                vector.behavior.say_text("We're both great!")
            else:
                vector.behavior.say_text(random.sample(ROBOT_LOSES, k=1)[0])
                robot_reaction = 'FrustratedByFailureMajor'
        else:
            vector.behavior.say_text(random.sample(ROBOT_WINS, k=1)[0])
        vector.anim.play_animation_trigger(robot_reaction)
        return robot_reaction


class CardMatrix:
    """
    Creating pseudo-random cards.

    Lotto cards are created according to certain rules that must be observed.
    For example, there should not be more than 5 filled cells in one row
    (or more than 2 in one column).

    Attributes:
        row_quantity (int): loto card rows quantity.
        cols_quantity (int): loto card columns quantity.
        max_row_num (int): maximum number of filled cells in a loto card row.
        max_col_num (int): maximum number of filled cells in a loto card
                           column.
    """

    row_quantity = 3
    cols_quantity = 9
    max_row_num = 5
    max_col_num = 2

    def __init__(self):
        """Initialize the card object."""
        self.body, self.str_body, self.all_card_numbers = [], [], []
        if not self.body:
            self.fill_body()

    def row_numbers(self, allowed_tens=None, row_numbers=None, tens=None):
        """
        Fill the row with numbers.

        Gets a pseudo-random number. Gets a digit of tens of this number.
        If the number is not in the ones already in the card and there is
        no digit of tens in stored tens, the number is added to the card
        (and to a row).
        If there is a number 90 in the row, its processing takes place
        (since it should, according to the rules of the game, get not
        into 10 tens, but into 9, unlike other numbers).

        Examples:
            [5, 38, 55, 60, 74]
            [4, 13, 61, 77, 86]
            [14, 26, 32, 47, 80]

        Args:
            allowed_tens (int or None): specified number's digit of tens.
            row_numbers (list  or None): specified or stored numbers
                                         in current row.
            tens (list  or None): specified or stored tens which have
                                  already been involved.

        Returns:
            list: sorted row of numbers.
        """
        row_numbers = [] if not row_numbers else row_numbers
        tens = [] if not tens else tens
        while len(row_numbers) < self.max_row_num:
            number = random.randint(1, 90)
            number_tens = number // 10
            if number_tens not in tens and number not in self.all_card_numbers:
                if allowed_tens and number_tens not in allowed_tens:
                    continue
                tens.append(number_tens)
                row_numbers.append(number)
                self.all_card_numbers.append(number)
        row_numbers.sort()
        if 90 in row_numbers and row_numbers[4]//10 == 8:
            row_numbers.pop(4)
            row_numbers = self.row_numbers(
                allowed_tens=allowed_tens, row_numbers=row_numbers, tens=tens
            )
        return row_numbers

    def fill_row(self, allowed_tens=None):
        """
        Fill current card row.

        Examples:
            [5, 0, 0, 38, 0, 55, 60, 74, 0]
            [4, 13, 0, 0, 0, 0, 61, 77, 86]
            [0, 14, 26, 32, 47, 0, 0, 0, 80]

        Args:
            allowed_tens (list): tens allowed to fill in this row.

        Returns:
            list: row filled with numbers and 0 (where no number in tens).
        """
        raw_row = (
            self.row_numbers(allowed_tens)
            if allowed_tens
            else self.row_numbers()
        )
        filled_row = []
        tens_map = list(range(0, 10))
        for number in raw_row:
            zero_map = tens_map.copy()
            for current_ten in zero_map:
                check = bool(number//10 != current_ten)
                recorded_value = 0 if check else number
                filled_row.append(recorded_value)
                tens_map.remove(current_ten)
                if not check:
                    break
        while len(filled_row) < self.cols_quantity:
            filled_row.append(0)
        if len(filled_row) == self.cols_quantity + 1:
            filled_row.pop(self.cols_quantity - 1)
        return filled_row

    def fill_body(self):
        """
        Fill the card body.

        Returns:
            None:
        """
        try:
            for counter in range(self.row_quantity):
                if counter == self.row_quantity - 1:
                    allowed_tens = self.cols_check()
                    self.body.append(self.fill_row(allowed_tens))
                    break
                self.body.append(self.fill_row())
            if self.sort_card():
                self.body_elems_to_str()
            else:
                self.body, self.str_body, self.all_card_numbers = [], [], []
                self.fill_body()
        except Exception as err:
            template = 'An exception of type {0} occurred. Arguments:\n{1!r}'
            message = template.format(type(err).__name__, err.args)
            logger.error(message)

    def body_elems_to_str(self):
        """
        Transform of all card numbers into a string type.

        Returns:
            None:
        """
        self.str_body = [
            [self.normalize_str_digits(number) for number in row]
            for row in self.body
        ]

    def cols_check(self, counter=0):
        """
        Select valid cells (tens of digits) to fill in the card last row.

        There can be 1 or 2 filled cells in one column. If two are
        already filled, this cell should remain empty in the third row.

        Args:
            counter (int): current tens.

        Returns:
            list: list of allowed tens to fill in the last row.
        """
        allowed_tens = []
        for i, j, in zip(*self.body):
            if i == 0 or j == 0:
                allowed_tens.append(counter)
            counter += 1
        return allowed_tens

    @staticmethod
    def normalize_str_digits(item):
        """
        Fill in the string output of each number.

        Args:
            item (int): current number.

        Returns:
            str: number's string output in the card.
        """
        if item == 0:
            return '  '
        if item//10 > 0:
            return str(item)
        return f'0{item}'

    def sort_card(self):
        """
        Sort card numbers.

        Sort the rows so that the numbers in the columns are
        from larger to smaller.

        Notes:
            f, s, t - first, second, third number.

        Examples:
            initial rows:
                [[0, 16, 22, 0, 0, 57, 0, 77, 83],
                [9, 0, 27, 30, 46, 54, 0, 0, 0],
                [6, 0, 0, 0, 42, 0, 61, 73, 86]]
            sorted rows:
                [[0, 16, 22, 0, 0, 54, 0, 73, 83],
                [6, 0, 27, 30, 42, 57, 0, 0, 0],
                [9, 0, 0, 0, 46, 0, 61, 77, 86]]

        Notes:
            If all numbers in a column are 0, the card will be re-generated.

        Returns:
            bool: checking and sorting result.
        """
        sorted_values = []
        for f, s, t in zip(*self.body):
            if all(val == 0 for val in (f, s, t)):
                return False
            if f:
                if s and s < f:
                    f, s = s, f
                elif t and t < f:
                    f, t = t, f
            elif s:
                if t and t < s:
                    s, t = t, s
            sorted_values.append((f, s, t))
        self.body = [list(i) for i in zip(*sorted_values)]
        return True

    def __str__(self):
        """Lotto card's output."""
        underline = ' '
        result = f'|{underline*6}==LOTTO CARD=={underline*6}|\n'
        for row in self.str_body:
            underline = ('---' * (len(row)) + '-')
            row_numbers = '|'.join(row)
            result += f'{underline}\n|{row_numbers}|\n'
        return f'{underline}\n{result}{underline}\n'


test_card = CardMatrix()
print(test_card)
