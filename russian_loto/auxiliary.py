"""Auxiliary game methods."""
from russian_loto.russian_loto_game import CardMatrix


def collect_player_info(card_quantity):
    """
    Generate and collect cards bodies and all card numbers.

    The bodies of the cards are necessary for the correct output
    of the cards in the template, and all the numbers are needed
    for the robot's reaction to the called game move number.

    Args:
        card_quantity (int): quantity of the current game cards.

    Returns:
        tuple[list, set]: cards collected data.
    """
    card_bodies, card_numbers = [], []
    for _ in range(card_quantity):
        card = CardMatrix()
        card_bodies.append(card.body)
        card_numbers.extend(card.all_card_numbers)
    return card_bodies, set(card_numbers)
