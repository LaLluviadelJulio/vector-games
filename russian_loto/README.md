# Russian Loto (Bingo Game) with Vector

![russian_loto](public/russian_loto.jpg)

## The point of the game / _Суть игры_

You and the robot receive cards with numbers to fill out.
The number is filled in after it has been named. The host of the game
(robot Vector) calls the dropped numbers in order. The first one who
fills in all his cards wins.

_Вы и робот получаете карточки с числами, которые следует заполнить.
Закрашивание числа происходит после того, как оно было названо.
Ведущий игры (робот Вектор) называет выпавшие номера по порядку.
Первый, кто заполнит все свои карточки, выигрывает._

Learn more about Vector: https://www.anki.com/en-us/vector

Learn more about the SDK: https://developer.anki.com/

## Game process / _Процесс игры_

### Launch and Settings / _Запуск и настройки_

After starting the game, a connection with the robot will 
be made (by default, up to 10 attempts).
If the connection is successful, a browser window will 
open with the game's start screen, and Vector will inform
you that he is ready to play.

_После запуска игры будет произведено соединение с роботом 
(по умолчанию до 10 попыток).
При успешном соединении откроется окно браузера со стартовым 
экраном игры, а Вектор сообщит, что он готов играть._

![start](public/start.png)

The start screen allows you to select three options:
* number of cards (from 1 to 3);
* game speed (from 2 to 5);
* play with or without Vector.

_Стартовый экран позволяет выбрать три опции:_
* _количество карточек (от 1 до 3);_
* _скорость игры (от 2 до 5);_
* _играть с Вектором или без._

The speed of the game is measured in seconds passing between
two named numbers.
It affects the robot's behavior: in mode 2, the robot will 
only call the numbers that have dropped out, and in mode 3 
or more, it will allow itself to make happy comments if a
number that is in its cards has dropped out.

_Скорость игры измеряется в секундах, проходящих между двумя 
названными числами.
Она влияет на поведение робота: при режиме 2 робот будет 
только называть выпавшие числа, а при режиме 3 и более 
позволит себе отпускать довольные комментарии, если выпало
число, которое есть в его карточках._

You can play without voicing the Vector of the drop-down 
numbers, to do this, uncheck the checkbox of the 
corresponding setting.

_Вы можете играть без озвучивания Вектором выпадающих чисел,
для этого снимите галочку в чекбоксе соответствующей настройки._

### Game Screen / _Экран игры_

After clicking the Play button, the main game screen will appear.
Cards will be generated in the quantity specified by you.
On the left are the robot's cards, on the right are yours.

_После нажатия кнопки "Играть" появится основной экран игры.
Будут сгенерированы карточки в количестве, указанном вами.
Слева расположены карточки робота, справа - ваши._

![main](public/main.png)

At the top of the screen there are buttons for starting a 
new game and stopping the current one, as well as a field 
where the current number that has fallen out will be indicated.
At the bottom of the screen there is a progress bar, 
which will be filled during the game.

_В верхней части экрана находятся кнопки для начала новой игры и
остановки текущей, а также поле, где будет указано текущее выпавшее
число. В нижней части экрана расположен прогресс-бар, который будет заполняться
в процессе игры._

The robot will call the dropped numbers, if they are in its 
cards, they will be automatically filled in. You have to fill in 
them yourself by clicking on the desired cells. You can paint 
over only those cells that contain the current number or the 
numbers that dropped out earlier.

_Робот будет называть выпавшие числа, если они есть в его карточках, они будут
автоматически закрашены. Вам предстоит закрашивать их самостоятельно,
кликая на нужные клетки. Закрасить можно только те клетки, которые содержат
текущее число или числа, которые выпадали ранее._

The winner is the player who first fills in all the numbers 
of his cards. A message about who won will be displayed on 
the screen. Vector will definitely react to your win or loss!

_Победит тот игрок, который раньше закрасит все числа своих 
карточек. Сообщение о том, кто выиграл, будет выведено на экран.
Вектор обязательно отреагирует на свой выигрыш или проигрыш!_

![win](public/win.png)

If you want to start a new game, click the appropriate button.
In this case, all the progress of the game will be canceled, 
you will be taken to the start page and will be able to enter 
new settings.

_Если вы захотите начать новую игру, нажмите соответствующую кнопку.
При этом весь прогресс игры будет аннулирован, вы попадете на стартовую страницу
и сможете ввести новые настройки._

When you click the end game button, a message will appear 
about stopping the game. The script will be completely 
stopped, the connection with the robot is closed too, and it
will be impossible to start a new game without restarting 
the game script.

_При нажатии кнопки завершения игры появится сообщение об 
остановке игры. Скрипт будет завершен, соединение 
с роботом закрыто, новую игру при этом будет начать невозможно 
без повторного запуска скрипта игры._

![exit](public/exit.png)

## How to start the app / _Как запустить приложение_

### 0. Set up Vector / _Настройка Вектора_

If you haven't set up your Vector yet and don't know how to do it, follow all
[instructions](https://developer.anki.com/vector/docs/index.html).
Also create and run a virtual environment for game project and download the requirements:

_Если ваш Вектор еще не был настроен, и вы не знаете, как это сделать, используйте
руководство от разработчиков по ссылке [instructions](https://developer.anki.com/vector/docs/index.html).
Также вам потребуется создать и запустить виртуальное окружение для проекта и загрузить
необходимые пакеты:_

```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

### 1. Virtual environment / _Виртуальное окружение_

You should create an .env file in the game directory and put the following value in it:

_Следует создать файл .env в директории игры и поместить в него следующую строку:_
```
FLASK_SECRET_KEY = wefbewjhb67t6uew
```
___Do not use these key, it is given only as an example and will not work___

___Не используйте этот секретный ключ, он не будет работать и приведен только как пример___

FLASK_SECRET_KEY must be created manually.
There are many ways, one of them is to use the following code in the Python Console:

_FLASK_SECRET_KEY может быть создан вручную. Есть много вариантов для этого, например,
использовать в консоли следующий код:_

```python
import os
os.urandom(12)
```

### 2. Launch game / _Запуск игры_

Use command

_Используйте команду_
```
python3 run.py
```
in terminal or run the run.py file in any convenient way.

_в терминале или запустите файл run.py удобным вам способом._

### I hope your Vector likes the new game!

### _Надеюсь, вашему Вектору понравится новая игра!_


## Privacy Policy and Terms and Conditions

Use of Vector and the Vector SDK is subject to Anki's [Privacy Policy](https://www.anki.com/en-us/company/privacy) and [Terms and Conditions](https://www.anki.com/en-us/company/terms-and-conditions).

## License
<p><a name="license"></a></p>

Distributed under the MIT License.
