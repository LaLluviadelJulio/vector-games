"""Game app views."""

import logging
import os

from flask import (
    jsonify, redirect, render_template, request, url_for
)

from russian_loto import cache
from russian_loto.auxiliary import collect_player_info

from .run import loto_app
from .russian_loto_game import LotoGameManager

logger = logging.getLogger(__name__)


@loto_app.route('/', methods=['post', 'get'])
def index():
    """Start the game, specify the settings."""
    cache.clear()
    if request.method == 'POST':
        cards_quantity = request.form.get('cards_quantity')
        game_speed = request.form.get('game_speed')
        play_with_robot = request.form.get('play_with_robot')
        logger.info(
            f'Settings: {cards_quantity} cards, {game_speed} sec speed, '
            f'with Vector - {play_with_robot}'
        )
        return redirect(url_for(
            'game', cards_quantity=cards_quantity,
            play_with_robot=play_with_robot,
            game_speed=game_speed,
        ))
    return render_template(
        'start.html', title='Start Russian Loto',
    )


@loto_app.route('/call_number', methods=['get'])
def call_number():
    """
    Get the current number and the state of the game (over or not).

    Working with the cache (requesting and transmitting a game
    control object that stores called numbers and not yet called ones).
    """
    try:
        if not cache.get('numbers_manager'):
            numbers_manager = LotoGameManager()
            cache.set('numbers_manager', numbers_manager)
        else:
            numbers_manager = cache.get('numbers_manager')
        (current_number, game_over,
         called_numbers) = numbers_manager.call_a_number()
        cache.set('numbers_manager', numbers_manager)
    except Exception as err:
        template = 'An exception of type {0} occurred. Arguments:\n{1!r}'
        message = template.format(type(err).__name__, err.args)
        logger.error(message)
    return jsonify({
        'current_number': current_number,
        'game_over': game_over,
        'called_numbers': called_numbers,
    })


@loto_app.route('/game/', methods=['post', 'get'])
def game():
    """
    Game initialize.

    Creation of the specified number of cards for the player and the robot,
    saving to the cache.
    """
    cards_quantity = int(request.args.get('cards_quantity'))
    game_speed = int(request.args.get('game_speed'))
    try:
        your_cards, your_numbers = collect_player_info(cards_quantity)
        robot_cards, robot_nums = collect_player_info(cards_quantity)
        logger.info(
            f'Player all numbers: {your_numbers}, card bodies: {your_cards}'
        )
        logger.info(
            f'Vector all numbers: {robot_nums}, card bodies: {robot_cards}'
        )
        cache.set('robot_nums', robot_nums)
        cache.set('robot_cards', robot_cards)
        cache.set('your_cards', your_cards)
        cache.set('game_speed', game_speed)
        cache.set('play_with_robot', request.args.get('play_with_robot'))
    except Exception as err:
        template = 'An exception of type {0} occurred. Arguments:\n{1!r}'
        message = template.format(type(err).__name__, err.args)
        logger.error(message)
    return render_template(
        'russian_loto.html', title='Russian Loto',
        your_cards=your_cards, robot_cards=robot_cards,
        game_speed=game_speed,
    )


@loto_app.route('/end_game', methods=['post'])
def get_robot_end_game_reaction():
    """Force the robot to react to the result of the game."""
    player_wins = request.form['player_wins']
    robot_wins = request.form['robot_wins']
    try:
        return cache.get('numbers_manager').robot_end_game_emotions(
            player_wins, robot_wins
        )
    except Exception as err:
        template = 'An exception of type {0} occurred. Arguments:\n{1!r}'
        message = template.format(type(err).__name__, err.args)
        logger.error(message)


@loto_app.get('/shutdown')
def shutdown():
    """View that triggers the end of the game."""
    print('Завершение игры...')
    os._exit(0)
    return 'Server shutting down...'
