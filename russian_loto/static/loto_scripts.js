// Scripts for the game process.

let pr_bar = document.getElementById("progressbar"); // progress bar object
let pr_width = 0; // progress bar width

let current_value = 0; // number of the current move

let called_numbers = []; // previously called numbers
let all_robot_elements = []; // all the numbers of robot cards
let guessed_robot_elements = []; // all the numbers that the robot called
let all_my_elements = []; // all the numbers of my cards
let guessed_my_elements = []; // all the numbers I called
let game_speed = $('#game_speed').text(); // game speed


$('.numbers', '#me').not(`[value='']`).click(function () {
        /**
         * Adding a number to the closed ones (called) by the player.
         * Checking whether it is possible to fill over the cell clicked
         * by the player (if its value is among the already called numbers).
         */
        $(this).each(function (i, obj) {
            let clicked_number = Number(obj.attributes.value.textContent)
            if (
                clicked_number === current_value ||
                called_numbers.includes(clicked_number)
            ) {
                obj.style.backgroundColor = 'grey';
                guessed_my_elements.push(clicked_number)
                guessed_my_elements.sort()
            }
        });
    }
);


function fill_all_nums_lists(elem_id, target_list) {
    /**
     * Filling in the list of all numbers of all cards of the player and the robot.
     * @param {string} elem_id - id of the html element to search for the necessary elements
     * @param {Array} target_list - list to fill in (list of all robot or player card numbers)
     */
    $('.numbers', elem_id).not(`[value='']`).each(function (i, obj) {
        let num = Number(obj.attributes.value.textContent)
        if (num !== 0) {
            target_list.push(num);
        }
    })
    target_list.sort()
}


function process_progress_bar(game_over) {
    /**
     * Filling in the progress bar.
     * @param {boolean} game_over - game over flag
     */
    if (game_over) {
        clearInterval(callNum);
        pr_bar.style.width = 100 + "%";
        pr_bar.innerText = 'Game over';
    } else {
        pr_width = pr_width + 1.11;
        pr_bar.style.width = pr_width + "%";
    }
}


function check_who_wins() {
    /**
     * Checking whether someone has already won on this turn of the game.
     * @returns {string} - result of the checking.
     */
    let robot_wins = (
        guessed_robot_elements.length === all_robot_elements.length
    ) && guessed_robot_elements.every(function (element, index) {
        return element === all_robot_elements[index];
    });
    let player_wins = (
        guessed_my_elements.length === all_my_elements.length
    ) && guessed_my_elements.every(function (element, index) {
        return element === all_my_elements[index];
    });
    if (player_wins || robot_wins) {
        $.post("/end_game", {
            'player_wins': player_wins,
            'robot_wins': robot_wins
        });
    }
    if (robot_wins) {
        if (player_wins) return 'Выиграли и вы, и Вектор!';
        else return 'Вектор выиграл!'
    }
    if (player_wins) return 'Вы выиграли!';
    else return 'Next'
}


function fill_robot_number(current_num) {
    /**
     * Filling in the matching robot numbers.
     */
    let robot_elements = $(`.numbers[value='${current_num}']`, '#robot');
    if (robot_elements.length > 0) {
        robot_elements.each(function (i, obj) {
            obj.style.backgroundColor = 'grey';
            guessed_robot_elements.push(Number(current_num))
            guessed_robot_elements.sort()
        });
    }
}


let callNum = setInterval(function () {
    /**
     * Main call number process.
     */
    if (all_robot_elements.length === 0 || all_my_elements.length === 0) {
        fill_all_nums_lists('#robot', all_robot_elements)
        fill_all_nums_lists('#me', all_my_elements)
    }
    let game_over = false;
    let result = check_who_wins();
    if (!(result === "Next")) {
            game_over = true;
            alert(result)
            process_progress_bar(game_over)
    }
    else{
        $.get('/call_number').done(function (response) {
            let elem_number = document.getElementById('current_number')
            elem_number.innerHTML = current_value = response['current_number']
            called_numbers = response['called_numbers']
            fill_robot_number(current_value)
            process_progress_bar(game_over)
        }).fail(function () {
            alert('Извините, что-то пошло не так. Попробуйте начать новую игру.')
            window.location = '/';
        });
    }
}, game_speed * 1000);

$(document).on('click', '#exit_game', () => {
    /**
     * Completion of the game. Stopping the Flask server.
     */
    clearInterval(callNum);
    $.get('/shutdown');
    setTimeout(function () {
        alert('Игра остановлена')
    }, 1500);
});
