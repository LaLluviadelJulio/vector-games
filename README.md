<!-- PROJECT SHIELDS -->
[![MIT License][license-shield]][license-url]
<p><a name="readme-top"></a></p>


<!-- PROJECT LOGO -->
<br />
<div align="center">
  <h3 align="center">Vector Games</h3>

  <p align="center">
    Games that can be played with a robot Vector
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li><a href="#games">Games</a></li>
      <ul>
        <li><a href="#fortune_wheel">Fortune Wheel</a></li>
        <li><a href="#russian_loto">Russian Loto</a></li>
      </ul>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->
## About The Project
<p><a name="about-the-project"></a></p>

Simple games that you can play together with your robot 
Anki Vector. He will voice the progress of each game and 
react emotionally to its result.

_Несложные игры, в которые вы можете поиграть вместе со 
своим роботом Анки Вектором. Он будет озвучивать ход каждой 
игры и эмоционально реагировать на ее результат._

<p align="right">(<a href="#readme-top">back to top</a>)</p>



### Built With
<p><a name="built-with"></a></p>

[![Python][Python logo]][Python url]
[![Flask][Flask logo]][Flask url]
[![JavaScript][JavaScript logo]][JavaScript url]
[![JQuery][JQuery logo]][JQuery url]
[![Anki Vector SDK][Anki Vector SDK logo]][Anki Vector SDK url]

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- MAIN INFO -->
## Games
<p><a name="games"></a></p>

### Fortune Wheel

<p><a name="fortune_wheel"></a></p>

[Fortune Wheel docs »](https://gitlab.com/LaLluviadelJulio/vector-games/-/blob/main/fortune_wheel/README.md)

A game in which Vector helps you guess the hidden English word . 
All you see on the screen are empty cells (according to the 
number of letters in the hidden word) and its translation. 
One by one you choose a letter until the word is guessed.

_Игра, в которой Вектор помогает вам угадывать загаданное 
английское слово. Все, что вы видите на экране - пустые 
клеточки (по количеству букв в загаданном слове) и его 
перевод. По очереди вы выбираете букву, пока слово не будет 
угадано._

![fortune_wheel](https://gitlab.com/LaLluviadelJulio/vector-games/-/raw/main/fortune_wheel/public/fortune_wheel.jpg)

### Russian Loto

<p><a name="russian_loto"></a></p>

[Russian Loto docs »](https://gitlab.com/LaLluviadelJulio/vector-games/-/blob/main/russian_loto/README.md)

In this game, you will be given from 1 to 3 cards with numbers 
to fill out. Vector will call the dropped numbers in turn and 
fill out his cards. You need to do the same. The winner is the 
one who fills out all the cards faster.

_В этой игре вам будет предоставлено от 1 до 3 карточек с числами, 
которые надо заполнить. Вектор будет называть выпавшие числа по 
очереди и заполнять свои карточки. Вам нужно сделать то же самое. 
Победит тот, кто быстрее заполнит все карточки._

![russian_loto](https://gitlab.com/LaLluviadelJulio/vector-games/-/raw/main/russian_loto/public/russian_loto.jpg)

<!-- LICENSE -->
## License
<p><a name="license"></a></p>

Distributed under the MIT License.

Используется лицензия MIT.

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- CONTACT -->
## Contact
<p><a name="contact"></a></p>

Marina Marmalyukova - inspiracion@yandex.ru

Project Link: [https://gitlab.com/LaLluviadelJulio/vector-games](https://gitlab.com/LaLluviadelJulio/vector-games)

<p align="right">(<a href="#readme-top">back to top</a>)</p>




<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[Python logo]: https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white
[Python url]: https://www.python.org/?hl=RU
[JavaScript logo]: https://img.shields.io/badge/JavaScript-DA45?style=for-the-badge&logo=javascript&logoColor=white
[JavaScript url]: https://developer.mozilla.org/en-US/docs/Web/JavaScript
[JQuery logo]: https://img.shields.io/badge/JQUery-07405E?style=for-the-badge&logo=jquery&logoColor=white
[JQuery url]: https://jquery.com/
[Flask logo]: https://img.shields.io/badge/Flask-218440?style=for-the-badge&logo=flask&logoColor=white
[Flask url]: https://flask.palletsprojects.com/
[Anki Vector SDK logo]: https://img.shields.io/badge/Anki_Vector_SDK-634890?style=for-the-badge&logoColor=white
[Anki Vector SDK url]: https://developer.anki.com/
